#!/usr/bin/ruby
=begin
Planter v1.3
Brett Terpstra 2013
ruby script to create a directory structure from indented data.

Three ways to use it:
- Pipe indented (tabs or 2 spaces) text to the script
  - e.g. `cat "mytemplate" | planter.rb
- Create template.tpl files in ~/.planter and call them by their base name
  - e.g. Create a text file in ~/.planter/site.tpl
  - `planter.rb site`
- Call planter.rb without input and it will open your $EDITOR to create the tree on the fly

You can put %%X%% variables into templates, where X is a number that corresponds to the index
of the argument passed when planter is called. e.g. `planter.rb client "Mr. Butterfinger"`
would replace %%1%% in client.tpl with "Mr. Butterfinger". Use %%X|default%% to make a variable
optional with default replacement.

If a line in the template matches a file or folder that exists in ~/.planter, that file/folder
will be copied to the destination folder.
=end
require 'yaml'
require 'tmpdir'
require 'fileutils'
require 'pry'

def get_hierarchy(input,parent=".",dirs_to_create=[])
  return if input.is_a? String
  return if input.is_a? Integer
  input.each do |dirs|
    if dirs.kind_of? Hash
      dirs.each do |k,v|
        k = k.tr(':', '')
        unless ['name', 'length'].include?(k)
          dirs_to_create.push(File.expand_path("#{parent}/#{k.strip}"))
          dirs_to_create = get_hierarchy(v,"#{parent}/#{k.strip}",dirs_to_create) unless get_hierarchy(v,"#{parent}/#{k.strip}",dirs_to_create).kind_of? NilClass
        end
      end
    elsif dirs.kind_of? Array
      dirs_to_create = get_hierarchy(dirs,parent,dirs_to_create)
    elsif dirs.kind_of? String
      dirs_to_create.push(File.expand_path("#{parent}/#{dirs.strip}"))
    end
  end
  return dirs_to_create
end

def text_to_yaml(input, replacements = [])
  variables_count = input.scan(/%%\d+%%/).length
  if variables_count > replacements.length
    $stderr.puts('Mismatch variable/replacement counts!')
    $stderr.puts("Template has #{variables_count} required replacements, #{replacements.length} provided.")
    Process.exit 1
  end
  input.gsub!(/%%(\d+)(?:\|(.*?))?%%/) do |_match|
    if replacements[$1.to_i - 1]
      replacements[$1.to_i - 1]
    elsif !$2.nil?
      $2
    else
      print "Invalid variable"
      Process.exit 1
    end
  end
  lines = input.split(/[\n\r]/)
  prev_indent = 0
  lines.each_with_index do |line, i|
    indent = line.gsub(/  /,"\t").match(/(\t*).*$/)[1]
    if indent.length > prev_indent
      lines[i-1] = lines[i-1].chomp + ":"
    end
    prev_indent = indent.length
    lines[i] = indent.gsub(/\t/,'  ') + "- " + lines[i].strip # unless indent.length == 0
  end
  lines.delete_if {|line|
    line == ''
  }
  return "---\n" + lines.join("\n")
end

if STDIN.stat.size > 0
  data = STDIN.read
elsif ARGV.length > 0
  template = File.expand_path("~/.planter/#{ARGV[0].gsub(/\.tpl$/,'')}.tpl")
  ARGV.shift
  if File.exists? template
    File.open(template, 'r') do |infile|
      data = infile.read
    end
  else
    puts "Specified template not found in ~/.planter/*.tpl"
  end
else
  tmpfile = File.expand_path(Dir.tmpdir + "/planter.tmp")
  File.new(tmpfile, 'a+')

  # at_exit {FileUtils.rm(tmpfile) if File.exists?(tmpfile)}

  %x{$EDITOR "#{tmpfile}"}
  data = ""
  File.open(tmpfile, 'r') do |infile|
    data = infile.read
  end
end

data.strip!

yaml = YAML.load(text_to_yaml(data,ARGV))
# We also neeed YAML in form of hash so we can generate title files
yaml_hash = YAML.load(data,ARGV)
dirs_to_create = get_hierarchy(yaml)

dirs_to_create.each do |dir|
  curr_dir = ENV['PWD']
  unless File.exists? dir
    $stderr.puts "Creating #{dir.sub(/^#{curr_dir}\//,'')}"
    if File.exists?(File.join(File.expand_path("~/.planter"),File.basename(dir)))
      FileUtils.cp_r(File.join(File.expand_path("~/.planter"),File.basename(dir)), dir)
    else
      dir_name = dir.split('/').last
      Dir.mkdir(dir)
      path = dir.sub(/^#{curr_dir}\//,'').split('/')
      if dir_name.start_with?('chapter')
        FileUtils.cp(File.join(File.expand_path('./templates'), File.basename('chapter_title.md')), dir)
        # update title
        lines = File.readlines(File.join(dir, 'chapter_title.md'))
        lines[0] = "\# #{yaml_hash[path[0]]['name']}\n"
        lines[1] = "* #{yaml_hash[path[0]]['length']}"
        File.open(File.join(dir, 'chapter_title.md'), "w") { |f| f.write(lines.join) }
      elsif dir_name.start_with?('theme')
        FileUtils.cp(File.join(File.expand_path('./templates'), File.basename('theme_body.md')), dir)

        lines = File.readlines(File.join(dir, 'theme_body.md'))
        lines[0] = "\# #{yaml_hash[path[0]][path[1]]['name']}"
        File.open(File.join(dir, 'theme_body.md'), "w") { |f| f.write(lines.join) }
      elsif dir_name.start_with?('session')
        FileUtils.cp(File.join(File.expand_path('./templates'), File.basename('session_body.md')), dir)

        lines = File.readlines(File.join(dir, 'session_body.md'))
        lines[0] = "\# #{yaml_hash[path[0]][path[1]][path[2]]['name']}"
        File.open(File.join(dir, 'session_body.md'), "w") { |f| f.write(lines.join) }
      end
    end
  else
    $stderr.puts "Skipping #{dir.sub(/^#{curr_dir}\//,'')} (file exists)"
  end
end
