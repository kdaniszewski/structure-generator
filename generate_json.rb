#!/usr/bin/ruby
# Quick and easy way to convert from yaml to json with pretty format
# Usage:
# Example 1:
# 
# # Explicitly give the output filename
# ./yaml_to_json.rb input_file/name.yaml outputfile/name.json
# 
# Example 2:
# 
# # Will implicitly use the input file name with .json extension
# ./yaml_to_json.rb inputfilename.yaml
# 
# Example 3:
# 
# # Also works for .yml extension
# ./yaml_to_json.rb inputfilename.yml
# 
require 'yaml'
require 'tmpdir'
require 'fileutils'
require 'pry'
require 'json'

#list chapters in current directory
dirs = Dir.glob('*').select {|f| File.directory? f}
dirs = dirs.sort
dirs.delete('templates')
data = {}

dirs.each do |dir|
  # put chapter title and length into hash
  lines = File.readlines(File.join(File.expand_path('.'), dir, 'chapter_title.txt'))
  data[dir] = {}
  data[dir]['name'] = lines[0].tr('# ', '').tr("\n", '')
  data[dir]['length'] = lines[1].tr('* ', '')
  # list themes and fetch their data
  Dir.chdir(dir)
  themes = Dir.glob('*').select {|f| File.directory? f}
  themes = themes.sort

  themes.each do |theme|
    lines = File.readlines(File.join(File.expand_path('.'), theme,  'theme_body.txt'))
    data[dir][theme] = {}
    string_contents = File.read(File.join(File.expand_path('.'), theme,  'theme_body.txt'))
    # p contents
    # regex for extracting goals
    regex_title = /^(#)\s.*/
    regex_goals = /^(\*)\s(.*)$/
    regex_subtitle = /^(##)\s.*$/

    lines.each do |line|
      unless line.match(regex_title).nil?
        data[dir][theme]['name'] = line.match(regex_title)[0].tr('#', "").lstrip
        lines.delete(line)
      end
    end
    data[dir][theme]['goals'] = []
    lines.each do |line|
      # We have caught title and removed it from array. Now goals
      unless line.match(regex_goals).nil?
        data[dir][theme]['goals'] << line.match(regex_goals)[0].tr('*', '').lstrip
      end
    end

    string_contents = string_contents.split(regex_subtitle).drop(1).reject { |key| key == '##' }
    data[dir][theme]['good_to_know'] = string_contents[0]
    data[dir][theme]['session_overview'] = string_contents[1]

    # Sessions
    Dir.chdir(theme)
    sessions = Dir.glob('*').select {|f| File.directory? f}
    sessions = sessions.sort

    sessions.each do |session|
      lines = File.readlines(File.join(File.expand_path('.'), session, 'session_body.txt'))
      data[dir][theme][session] = {}
      string_contents = File.read(File.join(File.expand_path('.'), session, 'session_body.txt'))

      lines.each do |line|
        unless line.match(regex_title).nil?
          data[dir][theme][session]['name'] = line.match(regex_title)[0].tr('#', "").lstrip
          lines.delete(line)
        end
      end
      data[dir][theme][session]['goals'] = []
      lines.each do |line|
        # We have caught title and removed it from array. Now goals
        unless line.match(regex_goals).nil?
          data[dir][theme][session]['goals'] << line.match(regex_goals)[0].tr('*', '').lstrip
        end
      end

      string_contents = string_contents.split(regex_subtitle).drop(1).reject { |key| key == '##' }
      data[dir][theme][session]['good_to_know'] = string_contents[0]
      data[dir][theme][session]['exploration'] = string_contents[1]
      data[dir][theme][session]['discussion'] = string_contents[2]
      data[dir][theme][session]['practice'] = string_contents[3]
      data[dir][theme][session]['assesment'] = string_contents[4]


    end
    Dir.chdir('..')
  end
  # reset directory pointer
  Dir.chdir('..')
end

# puts data
output_filename = 'schema.json'

File.open(File.join(File.expand_path('.'), output_filename),"w") do |f|
  f.write(data.to_json)
end
