require 'tmpdir'
require 'fileutils'
require 'pry'
require 'json'
require 'csv'

extracted_data   = CSV.table(File.join(File.expand_path('.'), 'templates', 'sources', 'tool_types.csv'))
transformed_data = extracted_data.map { |row| row.to_hash }
final_data = {}
transformed_data.each do |activity|
    key = activity[:id]
    activity.delete(:id)
    final_data[key] = {}
    final_data[key] = activity
end

output_filename = 'activities.json'

File.open(File.join(File.expand_path('.'), output_filename),"w") do |f|
  f.write(final_data.to_json)
end